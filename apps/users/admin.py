from django.contrib import admin
from .models import User, Admins

admin.site.register(User)
admin.site.register(Admins)
